''' Demo program for sklearn where you just feed in random values of hyper-parameters and log to a model so
that you have multiple models

Create a script which has range of values for hyper-parameters of Random Forest

Select random combination of features for a model

try to write code that you can either run it for a single run or have a commented version which can
run for multiple times

Record metrics, parameters and artififacts(Plots, model, etc.) for each run '''



import os
import warnings
import sys
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split, StratifiedShuffleSplit
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report
import mlflow
import mlflow.sklearn
from sklearn.tree import DecisionTreeClassifier
import seaborn as sns
import logging
logging.basicConfig(level=logging.WARN)
import matplotlib.pyplot as plt
logger = logging.getLogger(__name__)



data = pd.read_csv('covtype.csv')

training_set, test_set = train_test_split(data,test_size=0.3, random_state = 42)

#Model Selection

# Number of trees in random forest
n_estimators = [int(x) for x in np.linspace(start = 200, stop = 2000, num = 10)]
np.random.choice(n_estimators)
# # Number of features to consider at every split
max_features = ['auto', 'sqrt']
# # Maximum number of levels in tree
max_depth = [int(x) for x in np.linspace(10, 110, num = 11)]
max_depth.append(None)
# # Minimum number of samples required to split a node
min_samples_split = [2, 5, 10]
# # Minimum number of samples required at each leaf node
min_samples_leaf = [1, 2, 4]
# # Method of selecting samples for training each tree
bootstrap = [True, False]
# # Create the random grid

def print_confusion_matrix(confus_mat, class_names, figsize = (8,5), fontsize=8):
    df_cm = pd.DataFrame(confus_mat, index=class_names, columns=class_names)
    fig = plt.figure(figsize=figsize)
    heatmap = sns.heatmap(df_cm, annot=True, fmt="d")
    heatmap.yaxis.set_ticklabels(heatmap.yaxis.get_ticklabels(), rotation=0, ha='right', fontsize=fontsize)
    heatmap.xaxis.set_ticklabels(heatmap.xaxis.get_ticklabels(), rotation=45, ha='right', fontsize=fontsize)
    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    saved_fig = 'fig_sa.png'
    plt.savefig(saved_fig)
    mlflow.log_artifact(saved_fig)
    plt.close()

# =============================================================================
# random_grid = {'n_estimators': np.random.choice(n_estimators),
#                 'max_features':np.random.choice(max_features),
#                 'max_depth':np.random.choice(max_depth),
#                 'min_samples_split': np.random.choice(min_samples_split),
#                 'min_samples_leaf': np.random.choice(min_samples_leaf),
#                 'bootstrap': np.random.choice(bootstrap)}
# # =============================================================================
# =============================================================================
# rlf = RandomForestClassifier()
# rlf.set_params(n_estimators =  800,
#                max_features = random_grid['max_features'],
#                 max_depth = random_grid['max_depth'],
#                 min_samples_split = random_grid['min_samples_split'],
#                 min_samples_leaf = random_grid['min_samples_leaf'],
#                 bootstrap = random_grid['bootstrap'],
#                 n_jobs = -1)
# 
# =============================================================================

for i in range(8):
    

    random_grid = { 'max_features':np.random.choice(max_features),
                   'max_depth':np.random.choice(max_depth),
                   'min_samples_split': np.random.choice(min_samples_split),
                   'min_samples_leaf': np.random.choice(min_samples_leaf)
                   }

    rlf = DecisionTreeClassifier()
    rlf.set_params(max_features = random_grid['max_features'],
                   max_depth = random_grid['max_depth'],
                   min_samples_split = random_grid['min_samples_split'],
                   min_samples_leaf = random_grid['min_samples_leaf'],
                   )


    rlf.fit(training_set.iloc[:,:-1],training_set.iloc[:,-1])
    res = rlf.predict(test_set.iloc[:,:-1])   

    repo = classification_report(test_set.iloc[:,-1], res,output_dict= True)
    
    print(i)


    with mlflow.start_run(): 
    
        mlflow.log_params(random_grid)
        mlflow.log_metric('accuracy', repo['accuracy'])
        mlflow.log_metric('weighted_precision', repo['weighted avg'].get('precision',{}))
        mlflow.log_metric('weighted_recall', repo['weighted avg'].get('recall',{}))
        mlflow.log_metric('weighted_f1-score', repo['weighted avg'].get('f1-score',{}))
        mlflow.sklearn.log_model(rlf,'Deci_classi')
        confusion_mat = confusion_matrix(test_set.iloc[:,-1], res, labels = np.unique(res))
    
        print_confusion_matrix(confusion_mat, np.unique(res))
    

    
    
    
 
    
    
    
    
    
# =============================================================================
#     count = 1
#     for values in repo.keys():
#         
#         if values == 'accuracy':
#             break
#         
#         for items in repo[values].keys():
#             
#             mlflow.log_metric(items, repo[values].get(items, {}),step=count)
#             print(repo[values].get(items, {}))
#             print(count)
#         count+=1
#         
#     
# =============================================================================
    

    
    
    
    
    
    
    
    
    
    
    
    
    
    
  
    
    
    
    
    